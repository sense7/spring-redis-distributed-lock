package com.stanwind.lock.anno;

import com.stanwind.lock.bean.LockAutoConfiguration;
import java.lang.annotation.*;
import org.springframework.context.annotation.Import;

/**
 * @summary 开启分布式锁注解 #{@link Lock}
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(LockAutoConfiguration.class)
public @interface EnableLock {

}
